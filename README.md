# url-checker



Introduction
============
url_checker is a restful endpoint url checker. Once urls are registered in application, application is going to check the health of url at intervals.


Usage
============
Register a url:

urlName: name of the website

urlDomain: domain of the website

frequency: interval per seconds

expectedStatus: status code

expectedString(optional): return from website


curl -H "x-api-key:xxxxxx" --request POST --data '{"urlName":"google","urlDomain":"http://www.google.com","frequency":"60","expectedStatus":"400","expectedString":""}' http://34.207.163.183/urlOps

Get list of urls:

curl -H "x-api-key:xxxxxx" --request GET http://34.207.163.183/urlOps

Local test:
$ . startUp.sh

Installation
============
$ pip install Flask flask_restful APScheduler requests peewee
SQLite3
Python3.6
